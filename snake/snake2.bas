1 rem License: GPLv3 or later
2 rem Author: miguel de dios
3 rem version: 20161107-0312
4 rem -----------------------
9 let w = 32: let h = 22
10 let dleft = 1: let dright = 2: let dup = 3:  let ddown = 4 
11 dim s(h,w)
12 let p = 0
13 let d = dright
14 let l = 3
15 let sx = (l - 1)
16 let sy = (int(h/2))
17 let z$ = chr$(143)
18 dim b((w*h) + 1)
19 for i = 1 to l
20 let b(i) = d
21 next i
22 let f = 0

100 paper 0: border 2: flash 0
101 cls
102 gosub 3000

300 let a$=INKEY$
301 if A$="q" then let d = dup
302 if A$="a" then let d = ddown
303 if A$="o" then let d = dleft
304 if A$="p" then let d = dright

500 if d = dup then let sy = sy - 1
501 if d = ddown then let sy = sy + 1
502 if d = dleft then let sx = sx - 1
503 if d = dright then let sx = sx + 1



700 if sy < 0 or sx < 0 or sy = 22 or sx = 32 then goto 5000
701 if s(sy + 1, sx + 1) = 1 then goto 5000
703 if s(sy + 1, sx + 1) = 2 then gosub 2000

749 ink 7: print at sy, sx; z$
750 let s(sy + 1, sx + 1) = 1
751 gosub 4000

800 if f = 0 then gosub 1000
801 let f = f - 1

900 goto 300

1000 let x = int(rnd * 32)
1001 let y = int(rnd * 22)
1002 if s(y + 1, x + 1) <> 0 then goto 900
1003 ink 4: print at y, x; z$
1004 let s(y + 1, x + 1) = 2
1005 let f = 20
1006 return

2000 let l = l + 3
2001 let p = p + 1
2002 return

3000 for i = 0 to (l - 1)
3001 ink 7: print at sy, i; z$
3002 let s(sy + 1, i + 1) = 1
3003 next i
3004 return

4000 let x = sx
4001 let y = sy
4002 for i = 1 to l
4003 let b(i + 1) = b(i)
4004 next i
4005 let b(1) = d
4006 for i = 2 to (l + 1)
4007 if b(i) = dup then let y = y + 1
4008 if b(i) = ddown then let y = y - 1
4009 if b(i) = dleft then let x = x + 1
4010 if b(i) = dright then let x = x - 1
4012 next i
4013 ink 0: print at y, x; z$
4014 let s(y + 1, x + 1) = 0
4015 return

5000 pause 180
5001 paper 0: border 0
5002 cls
5003 ink 2: flash 1: print "GAME OVER"
5004 ink 2: flash 0: print "POINTS " + p