1 rem License: GPLv3 or later
2 rem Author: miguel de dios
3 rem version: 20161106-0236
4 rem -----------------------
10 rem w - width
20 rem h - height
30 rem s - screen data
40 rem b - body snake the points of snake
50 rem l - long snake
60 rem x - long snake
70 rem sx - head snake pos x
80 rem sy - head snake pos y
90 rem d - dir snake 0=left 1=right 2=up 3=down
100 rem p - points
110 rem f - flag to set the next food  
120 rem ----------------------
130 cls
140 rem init vars
150 rem ----------------------
160 let w = 32: let h = 22
170 dim s(h,w)
180 let l = 3
190 let sx = l
200 let sy = (int(h/2))
210 dim b(w*h)
220 for i = 1 to (w*h)
230 let b(i) = -1
240 next i
250 for i = 1 to l
260 let b(i) = (int(h/2) * 100) + (i - 1)
270 let s(int(h/2) + 1, i + 1) = 1
280 let d = 1
290 let p = 0
300 let f = 0
310 let Z$ = chr$(143)
320 if f = 0 then gosub 720
330 let a$=INKEY$
340 if A$="q" then let d=2
350 if A$="a" then let d=3
360 if A$="o" then let d=0
370 if A$="p" then let d=1
380 if d = 2 then let sy = sy - 1
390 if d = 3 then let sy = sy + 1
400 if d = 0 then let sx = sx - 1
410 if d = 1 then let sx = sx + 1
430 rem colision snake
440 rem ----------------------
450 if sy < 0 or sx < 0 or sy = 22 or sx = 32 goto 610
460 if s(sy + 1, sx + 1) = 1 goto 610
470 if s(sy + 1, sx + 1) = 2 gosub 660
480 rem paint snake
490 rem ----------------------
500 let o = b(1)
510 for i = 1 to l
520 let b(i) = b(i+1)
530 next i
540 let b(l) = (sy * 100) + sx
550 print at int(o / 100), o  - (int(o / 100) * 100); " "
560 for i = 1 to l
570 print at int(o / 100), b(i)  - (int(b(i) / 100) * 100); Z$
571 let s(int(o / 100) + 1, b(i)  - (int(b(i) / 100) * 100) + 1) = 0
580 next i
590 rem
600 f = f - 1
601 goto 320
610 rem show game over
620 rem ----------------------
630 cls
640 print "Lost   Points: " + p
650 stop
660 rem eat snake
670 rem ----------------------
680 let l = l + 1
690 let s(sy + 1,sx + 1) = 0
700 print at sy, sx; " "
710 return
720 rem generate food
730 rem ----------------------
740 let t = 10
750 let x = int(rnd * 32)
760 let y = int(rnd * 22)
780 let t = t - 1
790 if t = 0 then goto 330
800 if s(y + 1, x + 1) <> 0 then goto 750 
810 print at y, x; "*"
820 let s(y + 1, x + 1) = 2
830 let f = 20
840 goto 330