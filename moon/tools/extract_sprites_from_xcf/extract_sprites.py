#! /bin/env python3

from gimpformats.gimpXcfDocument import GimpDocument
from gimpformats.GimpLayer import GimpLayer
from gimpformats.gimpXcfDocument import GimpGroup
import sys
import os
import re

def clean_sys_name(name):
	bad_chars = r'[<>:"/\\|?*\']'
	clean_name = re.sub(bad_chars, '_', name)
	return clean_name

def save_sprites(entry, directory=".", mode = None):
	if type(entry) == GimpGroup:
		sub_dir = f'{directory}/{clean_sys_name(entry.name)}'
		os.makedirs(sub_dir)
		for item in entry.children:
			save_sprites(item, sub_dir, mode)
	else:
		if mode is not None:
			if mode == "1":
				converted_image = entry.image.convert("L").convert("1")
			else :
				converted_image = entry.image.convert(mode)
			print(f'{entry.name} mode {mode}')
			with open(f'{directory}/{clean_sys_name(entry.name)}_{"bw" if mode == "1" else "grey"}.png', "wb") as f:
				converted_image.save(f)
		else:
			print(f'{entry.name}')
			with open(f'{directory}/{clean_sys_name(entry.name)}.png', "wb") as f:
				entry.image.save(f)

def main():
	sprites_xcf = GimpDocument(sys.argv[1])
	mode = None
	if len(sys.argv) < 3:
		save_sprites(sprites_xcf.walkTree())
		return
	if sys.argv[2] == "--grey":
		mode = "L"
	if sys.argv[2] == "--black_and_white":
		mode = "1"
	save_sprites(sprites_xcf.walkTree(), mode=mode)
	

if __name__ == "__main__":
	main()
