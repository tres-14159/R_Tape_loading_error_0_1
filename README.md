# R Tape loading error, 0:1

It is a personal repository (license GPLv3 or later) with works for
[ZX spectrum](https://en.wikipedia.org/wiki/ZX_Spectrum) ...maybe the
[Sinclair ZX Spectrum +2A](https://en.wikipedia.org/wiki/ZX_Spectrum#ZX_Spectrum_.2B2)
because I had this microcomputer when I was a spanish child.

## Why?
Maybe I near to have "morriña" (spanish word) about these golden times.
And as the f*cking hipsters listen music in vinyl discs, the hackers
needs to remember / re-learn / discover our epic past.

And I have unfinished business about the coding in machine-code and make
somethings more powerful that only with
[Basic language](https://en.wikipedia.org/wiki/Sinclair_BASIC)

## How?
Well, nowadays there are a lot of tools to make the life more easy,
there is a cross compilers such as [z88DK](https://en.wikipedia.org/wiki/Z88DK)
or other things.

But I will try to be pure as the water (maybe someday I will use other
things).

But not all remebers are pretty, when I was child, I could not save
any own code in a cassette, I do not know the reason (and
I did not know it), maybe my microcomputer had something bad or the
cassetes was very cheaper. And I remember how a small me in the
afternoons before of school wrote the code in the speccy and therefore
I wrote this code in paper with a pen...maybe in someplace there are a
lot of my old codes.

Well I will try to work near to these days, at the moment I use the next
only things from this future:

 * **GNU/Linux**: or another OS free software because *"Do you want to
   be another zombie?"* or worst *"Are you proud to be a apple
   zombie?"* .
 * **Text Editor**: in my case [vim](https://en.wikipedia.org/wiki/Vim_(text_editor))
   / [geany](https://en.wikipedia.org/wiki/Geany) of course free
   software.
 * **[zmakebas](https://packages.debian.org/sid/zmakebas)**: it is a
   tool to convert text files (in basic) to the cassete images for
   spectrum (tap files)....yes it is a free software.
 * **[Fuse](https://en.wikipedia.org/wiki/Fuse_(emulator))**: a free
 software emulator of several spectrum machines.

### Install in Debian/Ubuntu
```
$ sudo apt install fuse-emulator-gtk spectrum-roms zmakebas
```

## What?
The subprojects or works are separated in directories in this
repository.

 * **Maze:** the simplest maze game.
 * **Snake:** (not finish) the snake game version.
 * **Chip8:** (not finish) a emulator chip8 in basic for spectrum (maybe it will be not implemented...but...).

## Another thing?
 * [http://trastero.speccy.org/cosas/manuales/manuales.htm]() has a lot
   original manuals of Spectrum machines. (spanish book)
 * [https://openlibra.com/en/book/codigo-maquina-zx-spectrum-para-principiantes]()
   is a jewell. How had the changed my childhood change with this book?
   (spanish book)
