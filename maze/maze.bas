1 rem License: GPLv3 or later
2 rem Author: miguel de dios
3 rem version: 20161104-1412
4 rem -----------------------
10 let A=30: let B=20: let Z$ = chr$(143)
20 cls: dim Z(A,B)
22 for K=1 to A:let Z(K,1)=1
24 let Z(K,B)=1:next K
26 for L=1 to B: let Z(1,L)=1
28 let Z(A,L)=1: next L
30 let Q=1: let W=14
31 let Z(Q,W)=0
32 let Z(A, INT(RND * 15 + 3))=2
33 for L=1 to B: for K=1 to A
34 if Z(K,L)=1 then print at L,K;Z$
35 next K: next L
40 print at W,Q;"*"
50 if Z(Q,W)=1 then print "CRASH": stop
60 if Z(Q,W)=2 then print "BIEN HECHO": stop
70 let A$=INKEY$
80 let X=int(rnd * A+1)
90 let Y=int(rnd * B+1)
100 let Z(X,Y)=1: print at Y,X;Z$
110 if A$="a" then let W=W-1
120 if A$="z" then let W=W+1
130 if A$="m" then let Q=Q+1
140 goto 40